const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
var sprite = document.getElementById("sprite");
var center;
var size;

function draw() {
  resetCanvas();
  drawWorld();
}

function resetCanvas() {
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function drawWorld() {
  size = Math.floor(canvas.width / (WORLD_WIDTH + blockSize));
  center = (canvas.width - size * WORLD_WIDTH) / 2;
  for (let i = 0; i < WORLD_WIDTH; i++) {
    for (let j = 0; j < WORLD_HEIGHT; j++) {
      if (WORLD[i + "," + j] === "empty") {
        if ((i + j) % 2 === 0) {
          ctx.fillStyle = "#98CE35";
        } else {
          ctx.fillStyle = "#90C738";
        }
      } else if (WORLD[i + "," + j] === "wall") {
        ctx.fillStyle = "#000000";
      } else if (WORLD[i + "," + j] === "snake") {
        if (sprite.checked) {
          if ((i + j) % 2 === 0) {
            ctx.fillStyle = "#98CE35";
          } else {
            ctx.fillStyle = "#90C738";
          }
        } else {
          if ((i + j) % 2 === 0) {
            ctx.fillStyle = "#2A00DD";
          } else {
            ctx.fillStyle = "#2A00FF";
          }
        }
      } else if (WORLD[i + "," + j] === "food") {
        if (sprite.checked) {
          if ((i + j) % 2 === 0) {
            ctx.fillStyle = "#98CE35";
          } else {
            ctx.fillStyle = "#90C738";
          }
        } else {
          ctx.fillStyle = "#FF0000";
        }
      }
      ctx.fillRect(i * size + center, j * size, size, size);

      if (sprite.checked && WORLD[i + "," + j] === "food") {
        ctx.drawImage(
          image,
          0 * 64,
          3 * 64,
          64,
          64,
          i * size + center,
          j * size,
          size,
          size
        );
      }
    }
  }
  if (sprite.checked) {
    drawSnake();
  }
}

function drawSnake() {
  for (i = 0; i < SNAKE.length; i++) {
    if (i == SNAKE.length - 1) {
      //si tête
      drawHead();
    } else if (i == 0 && SNAKE.length != 1) {
      //si queue
      drawTail();
    } else {
      //sinon c'est le corps
      drawBody(i);
    }
  }
}

function drawHead() {
  let head = SNAKE.length - 1;
  let x = SNAKE[head][0];
  let y = SNAKE[head][1];
  if (direction == "down") {
    //si va vers bas
    HeadDirectionX = 4;
    HeadDirectionY = 1;
  } else if (direction == "up") {
    //si va vers haut
    HeadDirectionX = 3;
    HeadDirectionY = 0;
  } else if (direction == "right") {
    //si vas vers droite
    HeadDirectionX = 4;
    HeadDirectionY = 0;
  } else if (direction == "left") {
    //si va vers gauche
    HeadDirectionX = 3;
    HeadDirectionY = 1;
  } else {
    console.log("error head");
  }

  ctx.drawImage(
    image,
    HeadDirectionX * 64,
    HeadDirectionY * 64,
    64,
    64,
    x * size + center,
    y * size,
    size,
    size
  );
}

function drawTail() {
  let x = SNAKE[0][0];
  let y = SNAKE[0][1];
  let xNext = SNAKE[1][0];
  let yNext = SNAKE[1][1];

  if (yNext < y && xNext == x) {
    //si va vers haut
    tailDirectionX = 3;
    tailDirectionY = 2;
  } else if (yNext > y && xNext == x) {
    //si va vers bas
    tailDirectionX = 4;
    tailDirectionY = 3;
  } else if (xNext < x && yNext == y) {
    //si vas vers gauche
    tailDirectionX = 3;
    tailDirectionY = 3;
  } else if (xNext > x && yNext == y) {
    //si va vers droite
    tailDirectionX = 4;
    tailDirectionY = 2;
  } else {
    console.log("error tail");
  }

  ctx.drawImage(
    image,
    tailDirectionX * 64,
    tailDirectionY * 64,
    64,
    64,
    x * size + center,
    y * size,
    size,
    size
  );
}

function drawBody(i) {
  let x = SNAKE[i][0];
  let y = SNAKE[i][1];
  let xNext = SNAKE[i + 1][0];
  let yNext = SNAKE[i + 1][1];
  let xPrece = SNAKE[i - 1][0];
  let yPrece = SNAKE[i - 1][1];

  let xBodyDirection;
  let yBodyDirection;

  if (
    (xNext < x && xPrece == x && yNext == y && yPrece < y) ||
    (xNext == x && xPrece < x && yNext < y && yPrece == y)
  ) {
    //gauche / haut
    xBodyDirection = 2;
    yBodyDirection = 2;
  } else if (
    (xNext > x && xPrece == x && yNext == y && yPrece < y) ||
    (xNext == x && xPrece > x && yNext < y && yPrece == y)
  ) {
    //droite / haut
    xBodyDirection = 0;
    yBodyDirection = 1;
  } else if (
    (xNext < x && xPrece == x && yNext == y && yPrece > y) ||
    (xNext == x && xPrece < x && yNext > y && yPrece == y)
  ) {
    //gauche / bas
    xBodyDirection = 2;
    yBodyDirection = 0;
  } else if (
    (xNext > x && xPrece == x && yNext == y && yPrece > y) ||
    (xNext == x && xPrece > x && yNext > y && yPrece == y)
  ) {
    //droite / bas
    xBodyDirection = 0;
    yBodyDirection = 0;
  } else if (yNext == y && yPrece == y) {
    //gauche / droite
    xBodyDirection = 1;
    yBodyDirection = 0;
  } else if (xNext == x && xPrece == x) {
    //haut / bas
    xBodyDirection = 2;
    yBodyDirection = 1;
  } else {
    console.log("error body");
  }

  ctx.drawImage(
    image,
    xBodyDirection * 64,
    yBodyDirection * 64,
    64,
    64,
    x * size + center,
    y * size,
    size,
    size
  );
}
