function IASnakeGetDirection() {
  let position = shortestPathToFood();
  let x = position.split(",")[0];
  let y = position.split(",")[1];
  let nextPosition = [x, y];

  let head = SNAKE[SNAKE.length - 1];

  if (head[0] < nextPosition[0]) {
    direction = "right";
  } else if (head[0] > nextPosition[0]) {
    direction = "left";
  } else if (head[1] < nextPosition[1]) {
    direction = "down";
  } else if (head[1] > nextPosition[1]) {
    direction = "up";
  }

  return nextPosition;
}

function shortestPathToFood() {
  let head = SNAKE[SNAKE.length - 1];
  let foodPosition = FOOD;
  let foodX = foodPosition[0];
  let foodY = foodPosition[1];
  let headX = head[0];
  let headY = head[1];

  let graph = [];

  for (let i = 0; i < WORLD_WIDTH; i++) {
    for (let j = 0; j < WORLD_HEIGHT; j++) {
      if (WORLD[i + "," + j] === "empty" || WORLD[i + "," + j] === "food") {
        graph[i + "," + j] = 1;
      } else {
        graph[i + "," + j] = 0;
      }
    }
  }

  let start = [headX + "," + headY];
  let end = [foodX + "," + foodY];
  let visited = [start];
  let queue = [start];
  let distances = {};
  let predecessor = [];

  for (let i = 0; i < WORLD_WIDTH; i++) {
    for (let j = 0; j < WORLD_HEIGHT; j++) {
      distances[i + "," + j] = Infinity;
    }
  }

  distances[start] = 0;

  while (queue.length > 0) {
    let current = queue.shift();
    let neighbors = getNeighbors(current, graph);

    for (let i = 0; i < neighbors.length; i++) {
      let neighbor = neighbors[i];
      if (visited.indexOf(neighbor) === -1) {
        visited.push(neighbor);
        queue.push(neighbor);
        distances[neighbor] = distances[current] + 1;
        predecessor[neighbor] = current;
      }
    }
  }

  let distanceFood = distances[end];
  if (distanceFood === Infinity) {
    let x = parseInt(headX);
    let y = parseInt(headY);

    let head1 = [x + 1, y];
    let head2 = [x - 1, y];
    let head3 = [x, y + 1];
    let head4 = [x, y - 1];

    if (isValidPosition(head1)) {
      let head = head1.toString();
      return head;
    }
    if (isValidPosition(head2)) {
      let head = head2.toString();
      return head;
    }
    if (isValidPosition(head3)) {
      let head = head3.toString();
      return head;
    }
    let head = head4.toString();
    return head;
  }
  let nextPlace = null;
  if (distanceFood === 1) {
    nextPlace = end.toString();
  } else {
    for (let i = 0; i < distanceFood - 1; i++) {
      let next = predecessor[end];
      end = next;
      nextPlace = end;
    }
  }
  return nextPlace;
}

function getNeighbors(node, graph) {
  let neighbors = [];

  if (Array.isArray(node)) {
    node = node[0];
  }
  let x = parseInt(node.split(",")[0]);
  let y = parseInt(node.split(",")[1]);

  if (graph[x + 1 + "," + y] === 1) {
    neighbors.push(x + 1 + "," + y);
  }
  if (graph[x - 1 + "," + y] === 1) {
    neighbors.push(x - 1 + "," + y);
  }
  if (graph[x + "," + (y + 1)] === 1) {
    neighbors.push(x + "," + (y + 1));
  }
  if (graph[x + "," + (y - 1)] === 1) {
    neighbors.push(x + "," + (y - 1));
  }

  return neighbors;
}
