let WORLD = [];
let SNAKE = [];
let FOOD = [];
let direction = "right";
let WORLD_WIDTH = 0;
let WORLD_HEIGHT = 0;
var interval = null;

const image = document.getElementById("snakeImage");
var url = "http://127.0.0.1:5500/level/level1.json";

const gameOver = document.getElementById("game-over");

const startSettings = document.getElementById("startSettings");
const home = document.getElementById("home");
const levelChoice = document.getElementById("level-choice");
const game = document.getElementById("game");

startSettings.addEventListener("click", function () {
  levelChoice.style.display = "flex";
  home.style.display = "none";
});

const ia = document.getElementById("ia");
let speedValue = 0;
let blockSize = 20;

function startGame() {
  const hash = location.hash.substring(1);
  if (hash != "") {
    url = "http://127.0.0.1:5500/level/level" + hash + ".json";
  }

  levelChoice.style.display = "none";
  game.style.display = "flex";
  gameOver.style.display = "none";

  const speed = document.getElementById("speed");
  speedValue = (11 - speed.value) * 50;

  const sizeChoice = document.getElementById("size");
  blockSize = (10 - sizeChoice.value) * 3;

  score.innerHTML = "0";
  direction = "right";

  main(url);
}

function initialize(url) {
  fetch(url)
    .then(function (response) {
      if (response.ok) {
        return response.json();
      } else {
        throw "Error" + response.status;
      }
    })
    .then((data) => {
      // Initialize world
      WORLD_WIDTH = data.dimensions[0];
      WORLD_HEIGHT = data.dimensions[1];

      if (location.hash.substring(1) == 4) {
        data.walls = createRandomWall();
      }

      for (let i = 0; i < data.dimensions[0]; i++) {
        for (let j = 0; j < data.dimensions[1]; j++) {
          WORLD[i + "," + j] = "empty";
          if (data.walls.some((wall) => wall[0] === i && wall[1] === j)) {
            WORLD[i + "," + j] = "wall";
          }
        }
      }

      // Initialize snake
      WORLD[data.snake[0]] = "snake";
      SNAKE = [data.snake[0]];

      // Initialize food
      WORLD[newFood()] = "food";
    })
    .catch(function (error) {
      console.log(error);
    });
}

const highScore = document.getElementById("high-score");

function main(url) {
  initialize(url);
  highScore.innerHTML = window.localStorage.getItem("Score");
  interval = setInterval(step, speedValue);
}

var step = function () {
  let nextPosition = [];
  if (ia.checked) {
    nextPosition = IASnakeGetDirection();
  } else {
    direction = getDirection();
    nextPosition = getNextPosition(direction);
  }

  if (isValidPosition(nextPosition)) {
    if (isFood(nextPosition)) {
      growSnake(nextPosition);
    } else {
      moveSnake(nextPosition);
    }
  } else {
    clearInterval(interval);
    endGame();
  }

  draw();
};

function getDirection() {
  addEventListener("keydown", function (event) {
    if (event.key === "ArrowUp" && direction !== "down") {
      direction = "up";
    } else if (event.key === "ArrowDown" && direction !== "up") {
      direction = "down";
    } else if (event.key === "ArrowLeft" && direction !== "right") {
      direction = "left";
    } else if (event.key === "ArrowRight" && direction !== "left") {
      direction = "right";
    }
  });
  return direction;
}

function getNextPosition(direction) {
  let head = SNAKE[SNAKE.length - 1];
  let nextPosition = [];
  if (direction === "up") {
    nextPosition = [head[0], head[1] - 1];
  } else if (direction === "down") {
    nextPosition = [head[0], head[1] + 1];
  } else if (direction === "left") {
    nextPosition = [head[0] - 1, head[1]];
  } else if (direction === "right") {
    nextPosition = [head[0] + 1, head[1]];
  }
  return nextPosition;
}

function isValidPosition(position) {
  if (position[0] < 0 || position[0] > WORLD_WIDTH - 1) {
    return false;
  } else if (position[1] < 0 || position[1] > WORLD_HEIGHT - 1) {
    return false;
  } else if (WORLD[position] === "wall") {
    return false;
  } else if (WORLD[position] === "snake") {
    return false;
  } else {
    return true;
  }
}

function moveSnake(nextPosition) {
  WORLD[SNAKE.slice()[0]] = "empty";
  WORLD[nextPosition] = "snake";

  SNAKE.push(nextPosition);
  SNAKE.shift();
}

const score = document.getElementById("score");

function isFood(position) {
  if (WORLD[position] === "food") {
    newFood();
    score.innerHTML = parseInt(score.innerHTML) + 1;
    return true;
  } else {
    return false;
  }
}

function newFood() {
  let newFood = [];
  do {
    newFood = [
      Math.floor(Math.random() * WORLD_WIDTH),
      Math.floor(Math.random() * WORLD_HEIGHT),
    ];
  } while (
    WORLD[newFood] !== "empty" // Ajouter la condition pour pas se bloquer
  );
  FOOD = newFood;
  WORLD[newFood] = "food";
}

function growSnake(nextPosition) {
  WORLD[nextPosition] = "snake";
  SNAKE.push(nextPosition);
}

function endGame() {
  console.log("Game over!");
  var score1 = parseInt(score.innerHTML);
  var highScore1 = parseInt(highScore.innerHTML);

  if (score1 > highScore1) {
    window.localStorage.setItem("Score", score1);
  }
  document.getElementById("game-over").style.display = "flex";
  document.getElementById("score-game-over").innerHTML = score1;
  document.getElementById("high-score-game-over").innerHTML = highScore1;
}

function createRandomWall() {
  var newWalls = [];
  let numberOfWall = Math.floor(Math.random() * 10 + 20);
  for (i = 0; i < numberOfWall; i++) {
    do {
      newWalls[i] = [
        Math.floor(Math.random() * WORLD_WIDTH),
        Math.floor(Math.random() * WORLD_HEIGHT),
      ];
    } while (!isWallValid(newWalls[i]));
  }

  return newWalls;
}

function isWallValid(position) {
  let isValid = false;
  if (
    //les coins ne peuvent pas générer des murs
    !(position[0] == 0 && position[1] == 0) &&
    !(position[0] == 0 && position[1] == 1) &&
    !(position[0] == 1 && position[1] == 0) &&
    !(position[0] == WORLD_HEIGHT - 1 && position[1] == 0) &&
    !(position[0] == WORLD_HEIGHT - 1 && position[1] == 1) &&
    !(position[0] == WORLD_HEIGHT - 2 && position[1] == 0) &&
    !(position[0] == 0 && position[1] == WORLD_WIDTH - 1) &&
    !(position[0] == 0 && position[1] == WORLD_WIDTH - 2) &&
    !(position[0] == 1 && position[1] == WORLD_WIDTH - 1) &&
    !(position[0] == WORLD_HEIGHT - 1 && position[1] == WORLD_WIDTH - 1) &&
    !(position[0] == WORLD_HEIGHT - 1 && position[1] == WORLD_WIDTH - 2) &&
    !(position[0] == WORLD_HEIGHT - 2 && position[1] == WORLD_WIDTH - 1)
  ) {
    isValid = true;
  }

  return isValid;
}
